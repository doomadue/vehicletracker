/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package VehicleTracking;

import java.io.*;
import java.util.*;
import java.util.logging.*;

/**
 *
 * @author dusty_000
 */
public class Vehicle {
    /* Standard information for Vehicle Description */
    String name;
    private String make;
    private String model;
    private short year;
    
    /* Optional information - nice to have */
    private String licensePlate;
    private Double purchasePrice;
    private int purchaseMilage;
    private short purchaseYear;
    private short purchaseMonth;
    private short purchaseDay;
    private String Vin;
    short tireWidth;
    short tireRatio;
    short tireDiameter;
    
    private ArrayList <GasRecord> gasLog = new ArrayList <>();
    private ArrayList <MaintenanceRecord> maintLog = new ArrayList <>();
    
    String garage_filePath = System.getProperty("user.dir")+"\\Garage\\";
    
    /*** SETTERS ***/
    
    void setVehicleInfo (String name, short year, String make, String model)
    {
        this.name  = name;
        this.make  = make;
        this.model = model;
        this.year  = year;
    }
    void setVehicleInfo (short year, String make, String model)
    {
        this.name  = Short.toString(year)+"-"+make+" "+model;
        this.make  = make;
        this.model = model;
        this.year  = year;
    }
    
    void setLicensePlate (String licensePlate)
    {
        this.licensePlate = licensePlate;
    }
    
    void setVin (String s)
    {
        this.Vin = s;
    }
    
    void setPurchasePrice (Double price)
    {
        this.purchasePrice = price;
    }
    
    void setPurchasePrice (String price)
    {
        if(price.contains("$"))
        {
            price = price.replace("$","");
        }
        if(!price.isBlank())
        {
            this.purchasePrice = Double.valueOf(price);
        }
        else
        {
            this.purchasePrice = null;
        }
    }
    
    void setPurchaseDate (short year, short month, short day)
    {
        this.purchaseYear = year;
        this.purchaseMonth = month;
        this.purchaseDay = day;
    }
    
    void setPurchaseDate (String yyyyMmDd)
    {
        String regex;
        String[] dateStr = {"","",""};
        if (!yyyyMmDd.isBlank())
        {
            if (yyyyMmDd.length() > 8)
            {
                regex = Character.toString(yyyyMmDd.charAt(4));
                dateStr = yyyyMmDd.split(regex);
                
                this.purchaseYear = Short.valueOf(dateStr[0]);
                this.purchaseMonth = Short.valueOf(dateStr[1]);
                this.purchaseDay = Short.valueOf(dateStr[2]);
            }
            
            else
            {
                this.purchaseYear = Short.valueOf(yyyyMmDd.substring(0, 2));
                this.purchaseMonth = Short.valueOf(yyyyMmDd.substring(2, 4));
                this.purchaseDay = Short.valueOf(yyyyMmDd.substring(4, 8));                
            }
        }
        else
        {
            
        }
    }
        
    void setTireSize (short width, short ratio, short diameter)
    {
        this.tireWidth    = width;
        this.tireRatio    = ratio;
        this.tireDiameter = diameter;
    }
    
    void setTireSize (String tireDescription)
    {
        if (tireDescription.contains(" /"))
        {
            String[] temp1 = tireDescription.split(" /");
            if (temp1[1].contains(" "))
            {
                String[] temp2 = temp1[1].split(" ");
                this.tireWidth = Short.valueOf(temp1[0]);
                this.tireRatio = Short.valueOf(temp2[0]);
                this.tireDiameter = Short.valueOf(temp2[1]);
            }
        }
    }
    /*** END SETTERS ***/
    
    /*** GETTERS ***/
    String getVehName ()
    {
        return this.name;
    }
    
    String getVehMake ()
    {
        return this.make;
    }
    
    String getVehModel ()
    {
        return this.model;
    }
    
    short getVehYear ()
    {
        return this.year;
    }
    
    double getPurchasePrice ()
    {
        if(this.purchasePrice == null)
        {
            return 0.00;
        }
        else
        {
            return this.purchasePrice;
        }
    }
    
    int getPurchaseMilage ()
    {
        return this.purchaseMilage;
    }
    
    String getLicensePlate ()
    {
        return this.licensePlate;
    }
    
    String getVin ()
    {
        return this.Vin;
    }
    
    String getPurchaseDate ()
    {
        return String.join("/", Short.toString(this.purchaseYear),Short.toString(this.purchaseMonth),Short.toString(this.purchaseDay));
    }
    
    short getTireWidth ()
    {
        return this.tireWidth;
    }
    
    short getTireRatio ()
    {
        return this.tireRatio;
    }
    
    short getTireDiameter ()
    {
        return this.tireDiameter;
    }
    
    String getTireDescription ()
    {
        return Short.toString(this.tireWidth)+" /"+Short.toString(this.tireRatio)+" "+Short.toString(this.tireDiameter);
    }
    
    /*** END GETTERS ***/
    
    boolean isFirstGasEntry ()
    {
        return this.gasLog.isEmpty();
    }
    
    boolean isFirstMaintEntry ()
    {
        return this.maintLog.isEmpty();
    }
    
    void addGasEntry (GasRecord a)
    {
        this.gasLog.add(a);
    }
    
    void addMaintEntry (MaintenanceRecord a)
    {
        this.maintLog.add(a);
    }
    void setInfoFromFile(String carName) 
    {
        String fileName = garage_filePath.concat("Info\\"+carName+".csv");
        File file = new File(fileName);
        
        Scanner fs;
        try {
            fs = new Scanner(file);
            fs.useDelimiter(",");

            this.setVehicleInfo(fs.next(), Short.parseShort(fs.next()), fs.next(), fs.next());
            this.setPurchaseDate(fs.next());
            this.setPurchasePrice(fs.next());
            this.setVin(fs.next());
            this.setLicensePlate(fs.next());
            this.setTireSize(fs.next());
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Vehicle.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    void saveInfo ()
    {
        String fileName = garage_filePath.concat("Info\\"+this.getVehName()+".csv");
        String line = String.join(",", 
                this.getVehName(),
                Short.toString(this.getVehYear()),
                this.getVehMake(),
                this.getVehModel(),
                this.getPurchaseDate(),
                "$"+Double.toString(this.purchasePrice),
                this.getVin(),
                this.getLicensePlate(),
                this.getTireDescription()
        );
        
        FileWriter csvWriter;
        try {
            csvWriter = new FileWriter(fileName,false);
            csvWriter.append(line);
            csvWriter.flush();
            csvWriter.close();
        } catch (IOException ex) {
            Logger.getLogger(Vehicle.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
