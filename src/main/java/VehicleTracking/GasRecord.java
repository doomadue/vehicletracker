/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package VehicleTracking;

/**
 * 
 * @author dusty_000
 */
public class GasRecord {
    boolean isFilledFull; /* 1= filled full, 0=partial fill */
    float fuelIn; /* U.S. Gallons */
    int odometer; 
    float fillPrice; /* U.S. Dollars */
    private short day;
    private short month;
    private short year;
    
    /*** SETTERS ***/
    /**
     * Sets the log data needed to perform calculations
     * @param isFull true if the log entry was to tank fill (pump shutoff)
     * @param gallons fuel added in U.S. gallons
     * @param odometerEntry Odometer reading in Miles
     * @param price fill price in U.S. dollars
     */
    void setGasFillInfo (boolean isFull, float gallons, int odometerEntry, float price)
    {
        isFilledFull = isFull;
        fuelIn = gallons;
        odometer = odometerEntry;
        fillPrice = price;
    }
    
    /**
     * Sets the date of the log entry
     * @param dateYear 
     * @param dateMonth
     * @param dateDay 
     */
    void setGasLogDate (short dateYear, short dateMonth, short dateDay)
    {
        year  = dateYear;
        month = dateMonth;
        day   = dateDay;
    }
    /*** END SETTERS ***/
    
    /*** GETTERS ***/
    /**
     * Gets the Full or partial fill info about the log entry
     * @return true if the log entry was to tank fill (pump shutoff)
     */
    boolean getIsFull ()
    {
        return isFilledFull;
    }
    
    /**
     * Gets the fuel added info of the log entry
     * @return fuel added in U.S. gallons
     */
    float getFuelIn ()
    {
        return fuelIn;
    }
    
    /**
     * Gets the Odometer reading of the log entry
     * @return Odometer reading in Miles
     */
    int getOdometer ()
    {
        return odometer;
    }
    
    /**
     * Gets the fill price of the log entry
     * @return fill price in U.S. dollars
     */
    float getFillPrice ()
    {
        return fillPrice;
    }
    
    /**
     * Gets the Date of the fill entry in String format
     * @param joiner the delimiter that separates each element (e.g. "-" or "/")
     * @return date in string format using the designated joiner
     */
    String getDateStr (String joiner)
    {
        return String.join(joiner, Short.toString(year), Short.toString(month), Short.toString(day));
    }
    
    /**
     * Gets the Date of the fill entry in an array of shorts
     * @return short array {year, month, day}
     */
    short[] getDate ()
    {
        short date[] = {year, month, day};
        return date;
    }
}
